# KSDF - Louisville Muhammad Ali International Airport

**[OFFICIAL FORUM THREAD](https://forums.x-plane.org/index.php?/forums/topic/167284-ksdf-louisville-international/&)**

**[Official ZDP Discord](https://discord.gg/YADGVW8zWv)**

**Installation Instructions:**

Download and unzip the ksdf---louisville-international folder into your "Custom Scenery" folder

It is recommended to use x-organizer to ensure correct scenery load order. This may also be done manually by opening the scenery_packs.ini file in the "Custom Scenery" folder, and moving ksdf---louisville-international to the top of the list. 

**Required Libraries**

* ZDP Library v1.1+

* MisterX Library v2.0+

* SAM Library v2+

Please report any bugs on the official ZDP discord (linked above)

To conctact me about this repo please send me a message on Discord @StableSystem#2042

This repository and it's contents are protected under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
Assets used from other developers was done so with their knowledge and approval. 

**Credits**

ZDP Developers: StableSystem

Ortho source - [USGS High Resolution Orthoimagery (HRO)](https://www.usgs.gov/centers/eros/science/usgs-eros-archive-aerial-photography-high-resolution-orthoimagery-hro)

**Open Source Resources**

This scenery is entirely open source and I encourage people to look through my source if you are curious how something is done or want to make modifications. If you modify something and think it should be implemented, please send me a message and I'd be happy to merge it into the official release. 

KSDF full source - https://gitlab.com/StableSystem/ksdf---louisville-international
